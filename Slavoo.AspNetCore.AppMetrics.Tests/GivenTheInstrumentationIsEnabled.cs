﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using App.Metrics.Health;
using Microsoft.AspNetCore.Mvc.Testing;
using Slavoo.AspNetCore.AppMetrics.Tests.Environment;
using Xunit;

namespace Slavoo.AspNetCore.AppMetrics.Tests
{
    public class GivenTheInstrumentationIsEnabled : IClassFixture<TestWebApplicationFactory>
    {
        private readonly HttpClient _client;

        private readonly TestWebApplicationFactory _webApp;

        public GivenTheInstrumentationIsEnabled(TestWebApplicationFactory webApp)
        {
            _webApp = webApp;

            _webApp.SetupHealthChecks(HealthCheckResult.Healthy, HealthCheckResult.Healthy, HealthCheckResult.Healthy);

            _client = webApp.CreateClient();
        }

        [Theory]
        [InlineData("/_env", 200, "")]
        [InlineData("/_health", 200, "")]
        [InlineData("/_ping", 200, "")]
        [InlineData("/_prometheus", 200, "")]
        public async Task WhenIMakeGetRequest_ThenIGetExpectedResponse(string path, int statusCode, string partOfBody)
        {
            var response = await _client.GetAsync(path);

            Assert.Equal(statusCode, (int)response.StatusCode);

            Assert.Contains(partOfBody, await response.Content.ReadAsStringAsync());
        }

        [Fact]
        public async Task WhenHealthCheckFails_Then503IsReturned(){
            _webApp.SetupHealthChecks(HealthCheckResult.Healthy, HealthCheckResult.Unhealthy, HealthCheckResult.Healthy);

            var response = await _client.GetAsync("/_health");

            Assert.Equal(503, (int)response.StatusCode);
        }

        [Fact]
        public async Task WhenHealthCheckDegrades_Then200IsReturned(){
            _webApp.SetupHealthChecks(HealthCheckResult.Healthy, HealthCheckResult.Healthy, HealthCheckResult.Degraded);

            var response = await _client.GetAsync("/_health");

            Assert.Equal(200, (int)response.StatusCode);
        }
    }
}