using System;
using System.Threading.Tasks;
using App.Metrics.Health;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;

namespace Slavoo.AspNetCore.AppMetrics.Tests.Environment
{
    public class TestWebApplicationFactory : WebApplicationFactory<TestStartup>
    {
        private Func<HealthCheckResult> _healthCheckOne = HealthCheckResult.Healthy;

        private Func<HealthCheckResult> _healthCheckTwo = HealthCheckResult.Healthy;

        private Func<HealthCheckResult> _healthCheckThree = HealthCheckResult.Healthy;

        public void SetupHealthChecks(Func<HealthCheckResult> one, Func<HealthCheckResult> two, Func<HealthCheckResult> three){
            
            _healthCheckOne = one;

            _healthCheckTwo = two;
            
            _healthCheckThree = three;
        }

        protected override IWebHostBuilder CreateWebHostBuilder()
        {
            return new WebHostBuilder().UseStartup<TestStartup>();
        }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {

            var healthChecks = new[]{
                new HealthCheck("one", () => new ValueTask<HealthCheckResult>(_healthCheckOne())),
                new HealthCheck("two", () => new ValueTask<HealthCheckResult>(_healthCheckTwo())),
                new HealthCheck("three", () => new ValueTask<HealthCheckResult>(_healthCheckThree())),
            };

            builder.WithSlavooInstrumentation(healthChecks);
        }
    }
}