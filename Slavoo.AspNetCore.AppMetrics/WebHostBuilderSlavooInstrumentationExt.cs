﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Metrics;
using App.Metrics.AspNetCore;
using App.Metrics.Formatters.Prometheus;
using App.Metrics.Health;
using Microsoft.AspNetCore.Hosting;

namespace Slavoo.AspNetCore.AppMetrics
{
    public static class WebHostBuilderSlavooInstrumentationExt
    {
        public static IWebHostBuilder WithSlavooInstrumentation(this IWebHostBuilder builder, IEnumerable<HealthCheck> healthChecks)
        {

            var metrics = App.Metrics.AppMetrics.CreateDefaultBuilder()
                .OutputMetrics.AsPrometheusPlainText()
                .Build();

            return builder.ConfigureHealth(healthBuilder =>
                    {
                        healthBuilder
                            .HealthChecks.AddChecks(healthChecks)
                            .OutputHealth.AsJson();
                    }
                )
                .UseHealthEndpoints()
                .ConfigureMetrics(metrics)
                .UseMetrics((hostOpts, opts) =>
                {
                    opts.EndpointOptions = eo =>
                    {
                        eo.EnvironmentInfoEndpointEnabled = true;
                        eo.MetricsTextEndpointOutputFormatter = metrics.OutputMetricsFormatters.OfType<MetricsPrometheusTextOutputFormatter>().Single();
                        eo.MetricsEndpointEnabled = true;
                    };
                })
                .ConfigureAppHealthHostingConfiguration(healthHost =>
                {
                    healthHost.HealthEndpoint = "/_health";
                    healthHost.PingEndpoint = "/_ping";
                })
                .ConfigureAppMetricsHostingConfiguration(metricsHost =>
                {
                    metricsHost.EnvironmentInfoEndpoint = "/_env";
                    metricsHost.MetricsEndpoint = "/_metrics";
                    metricsHost.MetricsTextEndpoint = "/_prometheus";
                });
        }
    }
}
